#!/usr/bin/env bash

sudo apt update && sudo apt upgrade

sudo apt install build-essential curl neofetch iotop -y

curl https://deb.nodesource.com/setup_14.x | sudo -E bash

sudo apt update && sudo apt install -y nodejs

# optional: update npm
sudo npm i -g npm pm2@latest